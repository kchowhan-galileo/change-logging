import { Request, Response } from "express";

import logger from "../util/logger";

/**
 * GET /health
 * Health page.
 */
export const index = (req: Request, res: Response) => {
    logger.info("Health Endpoint Invoked");
    res.status(200).json({
        message: "I'm Healthy"
    });
};
